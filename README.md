Виконане завдання 2го етапу [Dev Challenge 12](https://devchallenge.it/)


##  Демо

You can see it [in action](https://quirky-hugle-f3d947.netlify.com/)

## Для запуску зібраного проекту:

`npm install -g serve`

`serve -s build`

## Для запуску із початкового коду в режимі розробника
`npm install`

`npm start`