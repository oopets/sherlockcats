import React from "react";
import { FilterContainer } from "../filter/filter";
import { MapComponent } from "../../components/map/map";
import { getPets, addPet, removePet } from "../../model/store";
import { InfoModalComponent } from "../../components/info-modal/info-modal";
import { CreateModalComponent } from "../../components/create-modal/create-modal";
import { searchByFilter } from "../../services/search";
import "./content-container.css";

const ContentContainer = class extends React.Component {
  constructor(props) {
    super(props);
    const pets = getPets();
    this.state = {
      filter: {},
      radius: 0,
      pets,
      selected: null,
      createOpened: false,
      createLatlng: null
    };
  }

  render() {
    const { selected, createOpened, filter, pets } = this.state;

    const _pets = searchByFilter(pets, filter);

    return (
      <div className="content-container">
        <div className="columns content-container__filter">
          <div className="column">
            <FilterContainer
              onSearch={({ filter, radius }) => {
                this.setState({ filter, radius });
              }}
            />
          </div>
        </div>
        <div className="columns">
          <div className="column" style={{ textAlign: "center" }}>
            <i className="fas fa-info is-size-5 has-text-info" />
            <span className="content-container__pet-info">
              Щоб{" "}
              <span className="has-text-info has-text-weight-semibold">
                додати тварину
              </span>{" "}
              в базу, клікніть на мапі на приблизному місці, де
              загубився/знайшовся улюбленець
            </span>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <MapComponent
              pets={_pets}
              searchRadius={this.state.radius}
              onPetClick={selected => {
                this.setState({ selected });
              }}
              onMapClick={latlng => {
                this.setState({ createOpened: true, createLatlng: latlng });
              }}
            />
          </div>
        </div>

        {selected ? (
          <InfoModalComponent
            pet={selected}
            onDelete={pet => {
              this.setState({ selected: null });
              this.setState({ pets: removePet(pet.id) });
            }}
            onClose={() => {
              this.setState({ selected: null });
            }}
          />
        ) : null}

        {createOpened ? (
          <CreateModalComponent
            onSave={pet => {
              pet.latlng = this.state.createLatlng;
              this.setState({ pets: addPet(pet) });
              this.setState({ createOpened: false });
            }}
            onClose={() => {
              this.setState({ createOpened: false });
            }}
          />
        ) : null}
      </div>
    );
  }
};

export { ContentContainer };
