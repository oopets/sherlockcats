import React from "react";
import { PetStatusSwitchComponent } from "../../components/pet-form/pet-status-switch";
import { PetTypeComponent } from "../../components/pet-form/pet-type";
import { PetBreedComponent } from "../../components/pet-form/pet-breed";
import { PetAgeComponent } from "../../components/pet-form/pet-age";
import { PetColorComponent } from "../../components/pet-form/pet-color";
import { SearchRadiusComponent } from "../../components/pet-form/search-radius";

import PropTypes from "prop-types";

const FilterContainer = class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: {
        _found: true,
        _notFound: true
      },
      radius: 0
    };
  }

  assignFilterPortion(portion) {
    this.setState({ filter: Object.assign({}, this.state.filter, portion) });
  }

  render() {
    const { filter, radius } = this.state;

    return (
      <React.Fragment>
        <div className="columns">
          <div className="column is-narrow">
            <h4 className="title is-size-4">Фільтр</h4>
          </div>
          <div className="column" />
          <div className="column is-narrow">
            <SearchRadiusComponent
              radius={radius}
              onChange={radius => this.setState({ radius })}
            />
          </div>
        </div>

        <div className="columns">
          <div className="column is-narrow">
            <PetStatusSwitchComponent
              _found={filter._found}
              _notFound={filter._notFound}
              onChange={status => {
                this.assignFilterPortion(status);
              }}
            />
          </div>
          <div className="column is-narrow">
            <PetTypeComponent
              onChange={type => {
                this.assignFilterPortion({ type, breed: "" });
              }}
            />
            <PetBreedComponent
              petType={filter.type}
              onChange={breed => {
                this.assignFilterPortion({ breed });
              }}
            />
          </div>
          <div className="column is-narrow">
            <PetAgeComponent
              age={filter.age || 0}
              onChange={age => {
                this.assignFilterPortion({ age });
              }}
            />
          </div>
          <div className="column is-narrow">
            <PetColorComponent
              color={filter.color}
              onChange={color => this.assignFilterPortion({ color })}
            />
          </div>
          <div className="column">
            <button
              className="button is-primary is-rounded"
              onClick={() => {
                this.props.onSearch({filter, radius});
              }}
            >
              Застосувати
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
};

FilterContainer.propTypes = {
  onSearch: PropTypes.func.isRequired
};

export { FilterContainer };
