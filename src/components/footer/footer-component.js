import React from "react";

export function FooterComponent() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="content has-text-centered">
          <p>
            <strong>Sherlock Cats</strong>{" "}made with 💙by{" "}__________
          </p>
        </div>
      </div>
    </footer>
  );
}
