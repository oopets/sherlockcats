import React from "react";
import "./header-component.css";
import logo from "./logo.svg";

const HeaderComponent = () => (
  <header className="search-header">
    <nav className="navbar" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item" href="">
          <img src={logo} className="search-logo" alt="Sherlock Cats logo" />&nbsp;
          <span className="is-size-4 has-text-weight-semibold has-text-grey">Шерлок Кіт</span>
        </a>

        <a
          role="button"
          className="navbar-burger"
          aria-label="menu"
          aria-expanded="false"
        >
          <span aria-hidden="true" />
          <span aria-hidden="true" />
          <span aria-hidden="true" />
        </a>
      </div>

      <div className="navbar-menu">
        <div className="navbar-start">
          <a className="navbar-item" href=""  aria-label="about">
            Про сервіс
          </a>
        </div>
      </div>
    </nav>
  </header>
);

export { HeaderComponent };
