import React from "react";
import addIcon from "./img/add.svg";
import dogIcon from "./img/dog1.svg";
import catIcon from "./img/cat.svg";
import parrotIcon from "./img/parrot1.svg";
import catdogIcon from "./img/catdog.png";
import PropTypes from "prop-types";
import { searchInRadius } from "../../services/search";

const L = window.L;

const MarkerIcon = L.Icon.extend({
  options: {
    iconSize: [50, 50], // size of the icon
    iconAnchor: [25, 25] // point of the icon which will correspond to marker's location
  }
});

const ICONS = {
  dog: new MarkerIcon({ iconUrl: dogIcon }),
  cat: new MarkerIcon({ iconUrl: catIcon }),
  parrot: new MarkerIcon({ iconUrl: parrotIcon }),
  catdog: new MarkerIcon({
    iconUrl: catdogIcon,
    iconSize: [80, 80]
  })
};

const AddIcon = L.Icon.extend({
  options: {
    iconSize: [50, 50],
    iconAnchor: [25, 50]
  }
});

const MapComponent = class extends React.Component {
  shouldComponentUpdate(prevProps) {
    return prevProps.pets !== this.props.pets;
  }

  searchInRadiusIfNeeded(pets) {
    if (this.props.searchRadius) {
      return searchInRadius(pets, this.props.searchRadius, this.map, L);
    }
    return pets;
  }

  componentDidUpdate() {
    this.layerGroup.clearLayers();
    this.layerGroup.removeFrom(this.map);

    this.layerGroup = addPetsToMap(
      this.searchInRadiusIfNeeded(this.props.pets),
      this.map,
      this.props.onPetClick
    );
  }

  clearAddPetLayer() {
    if (this.addPetLayer) {
      this.addPetLayer.clearLayers();
      this.addPetLayer.removeFrom(this.map);
    }
  }

  componentDidMount() {
    this.map = createMap("mapid");

    this.layerGroup = addPetsToMap(
      this.searchInRadiusIfNeeded(this.props.pets),
      this.map,
      this.props.onPetClick
    );

    this.map.on("click", ({ latlng }) => {
      this.clearAddPetLayer();

      const { lat, lng } = latlng;

      const marker = L.marker(latlng, {
        icon: new AddIcon({ iconUrl: addIcon })
      });
      this.addPetLayer = L.layerGroup([marker]).addTo(this.map);

      marker.bindTooltip("Натисніть, щоб додати тварину тут").openTooltip();
      marker.addEventListener("click", () => {
        this.props.onMapClick({ lat, lng });
        this.clearAddPetLayer();
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <div id="mapid" style={{ height: 500 }} />
      </React.Fragment>
    );
  }
};

function createMap(mapid) {
  const lat = 50.4001;
  const lng = 30.5034;

  const mymap = L.map("mapid").setView([lat, lng], 13);
  L.tileLayer(
    "https://api.mapbox.com/styles/v1/anpets/cjhwaljqx0ace2st2tgd492pf/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYW5wZXRzIiwiYSI6ImNqaHc3bTM2MDBnMXUzcG1tZ3l4cGF1ejIifQ.ufqzcQF-cYA-xL-ZfHo8uw",
    {
      attributionControl: false,
      maxZoom: 18,
      minZoom: 9
    }
  ).addTo(mymap);

  return mymap;
}

function addPetsToMap(pets, map, onClick) {
  const markers = pets.map(pet => {
    const marker = L.marker(pet.latlng, { icon: ICONS[pet.type] });
    marker.addEventListener("click", e => {
      onClick(pet);
    });
    return marker;
  });

  const layerGroup = L.layerGroup(markers).addTo(map);

  return layerGroup;
}

MapComponent.propTypes = {
  pets: PropTypes.array.isRequired,
  onMapClick: PropTypes.func.isRequired,
  onPetClick: PropTypes.func.isRequired,
  searchRadius: PropTypes.number
};

export { MapComponent };
