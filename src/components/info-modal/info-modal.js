import React from "react";
import PropTypes from "prop-types";
import "./info-modal.css";
import { PET_TYPES, PET_BREEDS, PET_COLORS } from "../../model/meta-model";

const InfoModalComponent = ({ pet, onClose, onDelete }) => (
  <div className="modal is-active">
    <div className="modal-background" />
    <div className="modal-card">
      <header className="modal-card-head">
        <p className="modal-card-title" />
        <button className="delete" aria-label="close" onClick={onClose} />
      </header>

      <section className="modal-card-body">
        <div className="level">
          <div className="level-item has-text-centered">
            <div>
              <p className="heading">{PET_TYPES[pet.type]}</p>
              <p className="title">{pet.name || "без імені"}</p>
            </div>
          </div>
          <div className="level-item has-text-centered">
            <div>
              <p className="heading">Порода</p>
              <p className="title">
                {pet.breed ? PET_BREEDS[pet.type][pet.breed] : "невідома"}
              </p>
            </div>
          </div>
          <div className="level-item has-text-centered">
            <div>
              <p className="heading">Статус</p>
              <p className="title">{pet.found ? "Знайдено" : "Загублено"}</p>
            </div>
          </div>
        </div>

        {!pet.photo ? null : (
          <div className="level">
            <div className="level-item">
              <img
                className="info-modal__pet-photo"
                alt="Pet"
                src={pet.photo}
              />
            </div>
          </div>
        )}

        <div className="level">
          <div className="level-left">
            {!pet.age ? null : (
              <div className="level-item">
                <span className="info-modal__label ">Вік:</span>{" "}
                <b>{pet.age}</b>&nbsp;рочки
              </div>
            )}

            {!pet.color ? null : (
              <div className="level-item">
                <span className="info-modal__label ">Колір:</span>{" "}
                <b>
                  <span style={{ color: PET_COLORS[pet.color].value }}>
                    &#9679;
                  </span>{" "}
                  {PET_COLORS[pet.color].name}
                </b>
              </div>
            )}
          </div>
        </div>

        <div className="level">
          <p>
            <span className="info-modal__label ">Контакти господара:</span>
            {pet.contact}
          </p>
        </div>
      </section>

      <footer className="modal-card-foot">
        <button
          className="button is-danger"
          onClick={() => {
            if (window.confirm("Ви впевнені, що хочете видалити?")) {
              onDelete(pet);
            }
          }}
        >
          Видалити
        </button>
        <button className="button is-text" onClick={onClose}>
          Закрити
        </button>
      </footer>
    </div>
  </div>
);

InfoModalComponent.propTypes = {
  pet: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
};

export { InfoModalComponent };
