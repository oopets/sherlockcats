import React from "react";

export function PetAgeComponent({ onChange, age }) {
  return (
    <React.Fragment>
      <label className="label" htmlFor="age">
        Вік (роки):
      </label>
      <div className="field">
        <div className="control">
          <input
            id="age"
            onChange={e => onChange(parseInt(e.target.value, 10))}
            value={age}
            className="input"
            type="text"
            placeholder="0"
          />
        </div>
      </div>
    </React.Fragment>
  );
}
