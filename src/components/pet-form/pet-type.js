import React from "react";
import { PET_TYPES } from "../../model/meta-model";

export function PetTypeComponent({ value, onChange }) {
  return (
    <div className="field">
      <div className="control">
        <div className="select is-primary">
          <select
            value={value}
            onChange={event => {
              onChange(event.target.value);
            }}
          >
            <option value={''}>Вид тварини</option>

            {Object.keys(PET_TYPES).map(type => (
              <option key={type} value={type}>
                {PET_TYPES[type]}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
}
