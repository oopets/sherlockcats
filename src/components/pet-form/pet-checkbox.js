import React from "react";

export function PetCheckboxComponent({ found, onChange, id }) {
  return (
    <div className="field" style={{ textAlign: "center", marginBottom: 20 }}>
      <input
        className="is-checkradio"
        type="checkbox"
        checked={found} 
        id={id}
        onClick={() => {
          onChange(!found)
        }}
      />
      <label htmlFor={id}>
        Я хочу повідомити про знайдену тварину
      </label>
    </div>
  );
}
