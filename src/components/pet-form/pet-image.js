import React from "react";
import PropTypes from "prop-types";

export class PetImageComponent extends React.PureComponent {
  state = {
    src: ""
  };

  gotImage() {
    // var preview = document.querySelector("img");
    var file = this.fileInput.files[0];
    var reader = new FileReader();

    reader.addEventListener(
      "load",
      () => {
        this.setState({ src: reader.result });
        this.props.onChange(reader.result);
      },
      false
    );

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  render() {
    return (
      <React.Fragment>
        <label className="label">Фото тварини:</label>
        <div className="field">
          <div className="file">
            <label className="file-label" style={{ marginRight: 5 }}>
              <input
                className="file-input"
                ref={input => {
                  this.fileInput = input;
                }}
                onChange={this.gotImage.bind(this)}
                type="file"
                name="resume"
              />
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fas fa-upload" />
                </span>
                <span className="file-label">Choose a file…</span>
              </span>
            </label>
            {!this.state.src ? null : (
              <img
                src={this.state.src}
                alt="Pet preview"
                style={{
                  maxHeight: 36,
                  width: "auto"
                }}
              />
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

PetImageComponent.propTypes = {
  onChange: PropTypes.func.isRequired
}
