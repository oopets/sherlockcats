import React from "react";

export function SearchRadiusComponent({ onChange, radius }) {
  return (
    <div className="">
      <label className="label" htmlFor="age" style={{ width: 400 }}>
        Шукати співпадіння в радіусі:{" "}
        <span className="has-text-info">
          {!radius || radius < 0.1 ? "не задано" : radius + " км"}
        </span>
      </label>
      <input
        className="slider is-fullwidth is-small is-circle"
        step="0.5"
        min="0"
        max="10"
        value={radius}
        onChange={e => {
          onChange(parseFloat(e.target.value));
        }}
        type="range"
      />
    </div>
  );
}
