import React from "react";

export function PetNameComponent({ onChange, name }) {
  return (
    <React.Fragment>
      <label className="label" htmlFor="name">
        Як звуть:
      </label>
      <div className="field">
        <div className="control">
          <input
            id="name"
            onChange={e => onChange(e.target.value)}
            value={name}
            className="input"
            type="text"
            placeholder="Кличка"
          />
        </div>
      </div>
    </React.Fragment>
  );
}
