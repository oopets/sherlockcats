import React from "react";

export function PetStatusSwitchComponent({ onChange, _found, _notFound }) {
  return (
    <React.Fragment>
      <div className="field">
        <input
          id="foundPets"
          type="checkbox"
          className="switch is-rounded"
          checked={_found}
          onChange={e => {
            onChange({
              _found: !_found
            });
          }}
        />
        <label htmlFor="foundPets">знайдених</label>
      </div>
      <div className="field">
        <input
          id="lostPets"
          type="checkbox"
          className="switch is-rounded"
          checked={_notFound}
          onChange={e => {
            onChange({
              _notFound: !_notFound
            });
          }}
        />
        <label htmlFor="lostPets">загублених</label>
      </div>
    </React.Fragment>
  );
}
