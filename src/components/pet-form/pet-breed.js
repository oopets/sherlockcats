import React from "react";
import { PET_BREEDS } from "../../model/meta-model";

export function PetBreedComponent({ petType, onChange }) {
  let breeds = null;

  if (petType) {
    breeds = PET_BREEDS[petType];
  }

  return (
    <div className="field">
      <div className="control">
        <div className="select is-primary">
          <select
            disabled={!breeds}
            onChange={event => {
              onChange(event.target.value);
            }}
          >
            <option value={''}>Порода</option>
            {!breeds
              ? null
              : Object.keys(breeds).map(b => (
                  <option key={b} value={b}>
                    {breeds[b]}
                  </option>
                ))}
          </select>
        </div>
      </div>
    </div>
  );
}
