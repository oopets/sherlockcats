import React from "react";

export function PetContactComponent({ contact, onChange }) {
  return (
    <React.Fragment>
      <label className="label" htmlFor="contact">
        Контактна інформація:
      </label>
      <div className="field">
        <div className="control">
          <textarea
            id="contact"
            onChange={e => onChange(e.target.value)}
            value={contact}
            className="textarea"
            placeholder="Обов'язково вкажіть як з Вами можливо зв'затися,
           наприклад номер факсу або ICQ"
          />
        </div>
      </div>
    </React.Fragment>
  );
}
