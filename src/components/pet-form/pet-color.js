import React from "react";
import { CirclePicker } from "react-color";
import { PET_COLORS } from "../../model/meta-model";

export function PetColorComponent({ onChange, color }) {
  return (
    <React.Fragment>
      <label className="label">Колір тварини:</label>
      <div className="field">
        <CirclePicker
          color={(color ? PET_COLORS[color].value : undefined)}
          onChangeComplete={color => {
            onChange(
              Object.entries(PET_COLORS).find(
                ([k, v]) => v.value === color.hex.toUpperCase()
              )[0]
            );
          }}
          colors={Object.values(PET_COLORS).map(v => v.value)}
        />
        <div style={{ paddingTop: 10, height: 40 }}>
          {!color ? null : (
            <button
              className="button"
              onClick={() => {
                onChange("");
              }}
            >
              <i className="fas fa-eraser" />&nbsp; Будь-який
            </button>
          )}
        </div>
      </div>
    </React.Fragment>
  );
}
