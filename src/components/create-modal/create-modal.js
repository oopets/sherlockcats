import React from "react";
import PropTypes from "prop-types";
import { PetTypeComponent } from "../../components/pet-form/pet-type";
import { PetBreedComponent } from "../../components/pet-form/pet-breed";
import { PetAgeComponent } from "../../components/pet-form/pet-age";
import { PetNameComponent } from "../pet-form/pet-name";
import { PetColorComponent } from "../../components/pet-form/pet-color";
import { PetCheckboxComponent } from "../pet-form/pet-checkbox";
import { PetContactComponent } from "../pet-form/pet-contact";
import { PetImageComponent } from "../pet-form/pet-image";

const CreateModalComponent = class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pet: {
        found: false
      }
    };
  }

  isTouched() {
    return this.state.pet.type;
  }

  isValid() {
    return this.isTouched() && this.state.pet.contact;
  }

  onClose() {
    if (this.isTouched()) {
      if (window.confirm("Ви впевнені?")) {
        this.props.onClose();
      }
    } else {
      this.props.onClose();
    }
  }

  assignPetPortion(portion) {
    this.setState({ pet: Object.assign({}, this.state.pet, portion) });
  }

  render() {
    const { onSave } = this.props;
    const { pet } = this.state;

    return (
      <div className="modal is-active">
        <div className="modal-background" />
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Додати тварину</p>
            <button
              className="delete"
              aria-label="close"
              onClick={() => this.onClose()}
            />
          </header>

          <section className="modal-card-body">
            <PetCheckboxComponent
              id="create-pet-checkbox"
              onChange={found => {
                this.assignPetPortion({ found });
              }}
              found={pet.found}
            />

            <div className="columns">
              <div className="column is-narrow">
                <PetTypeComponent
                  onChange={type => {
                    this.assignPetPortion({ type, breed: "" });
                  }}
                />
                <PetBreedComponent
                  petType={pet.type}
                  onChange={breed => {
                    this.assignPetPortion({ breed });
                  }}
                />
                <PetAgeComponent
                  age={pet.age || 0}
                  onChange={age => this.assignPetPortion({ age })}
                />
                <PetNameComponent
                  name={pet.name || ""}
                  onChange={name => this.assignPetPortion({ name })}
                />
              </div>
              <div className="column">
                <PetColorComponent
                  color={pet.color}
                  onChange={color => this.assignPetPortion({ color })}
                />
                <PetImageComponent
                  onChange={photo => this.assignPetPortion({ photo })}
                />
                <PetContactComponent
                  contact={pet.contact}
                  onChange={contact => this.assignPetPortion({ contact })}
                />
              </div>
            </div>
          </section>

          <footer className="modal-card-foot">
            <button
              className="button is-primary"
              onClick={() => {
                onSave(this.state.pet);
              }}
              disabled={!this.isValid()}
            >
              Зберегти
            </button>
            <button className="button is-text" onClick={() => this.onClose()}>
              Закрити
            </button>
          </footer>
        </div>
      </div>
    );
  }
};

CreateModalComponent.propTypes = {
  onSave: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export { CreateModalComponent };
