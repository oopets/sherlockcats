import React, { Component } from "react";
import { HeaderComponent } from "./components/header/header-component";
import { ContentContainer } from "./containers/content/content-container";
import { FooterComponent } from "./components/footer/footer-component";
import "../node_modules/bulma/css/bulma.min.css";

class App extends Component {
  render() {
    return (
      <div className="container is-widescreen">
        <HeaderComponent />
        <ContentContainer />
        <FooterComponent />
      </div>
    );
  }
}

export default App;
