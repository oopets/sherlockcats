import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "../node_modules/bulma-checkradio/dist/bulma-checkradio.min.css";
import "../node_modules/bulma-switch/dist/bulma-switch.min.css";
import "../node_modules/bulma-slider/dist/bulma-slider.min.css";
import "../node_modules/bulma-slider/dist/bulma-slider.min.js";

import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
