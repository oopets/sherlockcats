/**
 *
 * @param {array} pets list of pets to filter by the provided criterias
 * @param {object} filterObject criterias to filter by: you can specify any pet attribute
 * to use for comparison, e.g. color, you can also specify special attributes _found and _notFound to
 * specify if among all already filtered pets you want to include pets that are Found or/and Not Found.
 */
export function searchByFilter(pets, filterObject) {
  const filterKeys = Object.keys(filterObject);

  if (filterKeys.length === 0) return pets;
  if (!filterObject["_found"] && !filterObject["_notFound"]) return [];

  const filters = filterKeys.filter(
    k => k !== "_found" && k !== "_notFound" && !!filterObject[k]
  );

  let _pets = pets.slice();

  filters.forEach(key => {
    _pets = _pets.filter(pet => pet[key] === filterObject[key]);
  });

  if (filterObject["_found"] && filterObject["_notFound"]) {
    return _pets;
  } else {
    return _pets.filter(pet => {
      return filterObject["_found"] ? pet["found"] : !pet["found"];
    });
  }
}

/**
 *
 * @param {array} pets list of pets to search of matches
 * @param {number} radius radius for search in km
 * @param {object} map instance of Leaflet map
 * @param {object} Leaflet reference to leaflet library
 */
export function searchInRadius(pets, radius, map, Leaflet) {
  let _pets = [];

  pets.forEach(pet => {
    const petLatlng = Leaflet.latLng(pet.latlng.lat, pet.latlng.lng);

    const matched = pets.filter(_testedPet => {
      if (pet === _testedPet) {
        return false;
      }
      const isSimilar =
        pet.type === _testedPet.type &&
        pet.breed === _testedPet.breed &&
        pet.found !== _testedPet.found;

      if (!isSimilar) {
        return false;
      }

      const testedLatlng = Leaflet.latLng(
        _testedPet.latlng.lat,
        _testedPet.latlng.lng
      );

      const distance = map.distance(petLatlng, testedLatlng);

      return distance <= radius * 1000; // distance in meters
    });

    matched.forEach(matchedPet => {
      if (!_pets.includes(matchedPet)) {
        _pets.push(matchedPet);
      }
    });
  });

  return _pets;
}
