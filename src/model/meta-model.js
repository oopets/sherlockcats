export const PET_TYPES = {
  parrot: "Папуга",
  dog: "Пес",
  cat: "Кіт",
  catdog: "Кіт-Пес"
};

export const PET_COLORS = {
  green: {
    name: "Зелений",
    value: "#36B259"
  },
  black: {
    name: "Чорний",
    value: "#000000"
  },

  red: {
    name: "Червоний ",
    value: "#F76A67"
  },

  gray: {
    name: "Сірий",
    value: "#5E93A5"
  },
  yellow: {
    name: "Жовтий",
    value: "#E3DB38"
  },
  
  pink: {
    name: "Рожевий",
    value: "#FFAAFF"
  }
};

export const PET_BREEDS = {
  parrot: {
    budgerigar: "Хвилястий",
    lovebird: "Нерозлучник",
    cockatoo: "Какаду"
  },
  cat: {
    british: "Британець",
    persian: "Персидський",
    mainecoon: "Мейн кун"
  },
  dog: {
    shepherd: "Німецька вівчарка",
    beagle: "Бігль",
    pug: "Мопс"
  },
  catdog: {
    any: "Будь-яка"
  }
};
