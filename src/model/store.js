import { DEMO_DATA } from "./demo";
const PETS_ITEM = "PETS";

if (!localStorage.getItem(PETS_ITEM)) {
  console.log("First run, setting some demo data");
  localStorage.setItem(PETS_ITEM, JSON.stringify(DEMO_DATA));
}

export function getPets() {
  const pets = localStorage.getItem(PETS_ITEM);
  return (pets && JSON.parse(pets)) || [];
}

export function addPet(pet) {
  pet.id = new Date().getTime();
  const newPets = [pet].concat(getPets());
  localStorage.setItem(PETS_ITEM, JSON.stringify(newPets));

  return newPets;
}

export function removePet(id) {
  const newPets = getPets().filter(pet => pet.id !== id);
  localStorage.setItem(PETS_ITEM, JSON.stringify(newPets));

  return newPets;
}
