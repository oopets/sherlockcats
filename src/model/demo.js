export const DEMO_DATA = [
  {
    id: 1,
    name: "Сірко",
    type: "dog",
    breed: "pug",
    age: 2,
    color: "gray",
    found: true,
    latlng: {
      lat: 50.4001,
      lng: 30.5034
    },
    contact: "Юрій Луценко, Прокуратура",
    photo: 'http://runt-of-the-web.com/wordpress/wp-content/uploads/2012/10/halloween-pug-america.jpg'
  },
  {
    id: 2,
    name: "Мурлик",
    type: "cat",
    breed: "british",
    age: 0,
    color: "gray",
    found: true,
    latlng: {
      lat: 50.4001,
      lng: 30.5534
    },
    contact: "Володимир Ляшко, ВРУ",
    photo: 'https://i0.wp.com/theverybesttop10.com/wp-content/uploads/2015/03/Top-10-Cats-Who-Want-To-Be-Superheroes-6.jpg'
  },
  {
    id: 3,
    name: "Ракєта",
    type: "parrot",
    age: 0,
    breed: "lovebird",
    found: false,
    latlng: {
      lat: 50.4101,
      lng: 30.5534
    },
    contact: "Ілон Маск, Марс",
    photo: 'http://www.drronsanimalhospitalsimivalley.com/blog/wp-content/uploads/2014/06/Parrot-Outside-Cage.jpg'
  },
  {
    id: 4,
    name: "Грейт",
    type: "catdog",
    breed: "any",
    age: 10,
    found: false,
    latlng: {
      lat: 50.4101,
      lng: 30.4934
    },
    contact: "Дональд Трамп, Вашингтон"
  }
];
